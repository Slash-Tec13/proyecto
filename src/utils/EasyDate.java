package utils;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
public class EasyDate {
	private final LocalDateTime localDateTime;
	public static EasyDate now() {
		return new EasyDate();
	};
	public EasyDate() {
		this.localDateTime = LocalDateTime.now();
	}
	public static EasyDate today() {
		return new EasyDate(LocalDate.now(), LocalTime.of(0, 0, 0));
	};
	private EasyDate(LocalDate now, LocalTime of) {
		this.localDateTime = LocalDateTime.of(now,  of);
	}
	public EasyDate(int year, int month, int day) {
		this(LocalDate.of(year, month, day), LocalTime.of(0, 0, 0));
	}
	public EasyDate(int year, int month, int day, int hour, int minute, int second) {
		this(LocalDate.of(year,  month,  day), LocalTime.of(hour,  minute, second));
	}
	public EasyDate(String date) {
		assert date != null;
		String[] partes = date.split("[/--]");
		int year = Integer.parseInt(partes[0]);
		int month = Integer.parseInt(partes[1]);
		int day = Integer.parseInt(partes[2]);
		this.localDateTime = LocalDateTime.of(year,  month, day, 0, 0, 0);
	}
	@Override
	public EasyDate clone() {
		return new EasyDate(this.localDateTime.toLocalDate(), this.localDateTime.toLocalTime());
	}
	//public int getAño() {
	//	return localDateTime.get(GregorianCalendar.YEAR);
	//}
	//public int getMes() {
	//	return localDateTime.get(GregorianCalendar.MONTH) + 1;
	//}
	//public int getDia() {
	//	return localDateTime.get(GregorianCalendar.DAY_OF_MONTH);
	//}
}
