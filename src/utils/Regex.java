package utils; 
import java.util.regex.Pattern;
public class Regex{
	public static final String REGEX_MAIL = "[\\w-\\+]+(\\.[\\w-\\+]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2.})$";
	public static final String REGEX_PASSWORD = "[A-ZÑa-zñ0-9%&#_·]{6,18}";
	public static final String LETTERS_NIF = "TRWAGMYFPDXBNJZSQVHLCKE";
	public static final String REGEX_NIF = "0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]";
	public static final String REGEX_POSTAL_CODE = "\\d]{5}";
	public static final String REGEX_POSTAL_NUMBER = "\\d]{1,3}[\\w]";
	public static final String REGEX_PERSON_NAME = "^[A-ZÑ][/áéíóúña-z ]+";
	public static final String REGEX_SURNAMES = "^[A-ZÑ][/áéíóúña-z ]+";
	public static final String REGEX_LOCATION_NAME = "^[A-ZÑ][/áéíóúña-z ]+[ A-ZÑa-zñáéíóú]";
	public static final String REGEX_STREET_NAME = "^[A-ZÑ][/áéíóúña-z ]+";
		public static boolean isValidFormat(String texto, String regex) {
		return Pattern.compile(regex).matcher(texto).matches();
	}
}