package models.dataAccess;
import main.java.EasyDate;
import main.java.Mail;
import main.java.password;
import models.Session;
import models.User;
import models.Mail;
public class Data {
	public static int MAX_DATA = 10;
	private User[] UserData;
	private Session[] SessionData;
	private Simulation[] simulationsData;
	private int registerdSessions = 0;
	private int registerdUser;
	private int registerdSimulations;
	public Data() {
	this.usersData = new User[MAX_DATA];
	this.sessionsData = new Session[MAX_DATA];
	this.simulationsData = new Simulations[MAX_DATA];
	this.registerdUser = 0;
	this.registerdSessions = 0;
	this.registerdSimulations = 0;
	}
	private void loadIntegratedUsers() {
		this.createUser(new User(new Nif("00000000T"),
				"Admin",
				"Admin Admin",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("admin@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new password("Miau#00"), 
				"REGISTERED"
				);
		this.createUser(new User(new Nif("00000001R",
				"Guest",
				"Guest Guest",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("guest@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new password("Miau#00"), 
				"REGISTERED"
				);
	public User findUser(String id) {
	for (main.java.User user : UsersData) {
		if (user.getNif().getText().equals(id)) {
			return user;
		}
	}
		return null;
	}
	public void createUser(main.java.User user) {
		if (findUser(user.getNif().getText() == null){
		UsersData[registerdUser] = user;
		registerdUser++;				
		return
		}
	public User findUser(String id) {
	for (main.java.User user : usersData) {
		if (user.getNif().getText().equals(id)) {
			return user;
		}
	}
	return null;
	}
	public void updateUser(main.java.User user) {
		User userOld = findUser(user.getNif().getText());
		if (userOld != null) {
			usersData[this.indexOfUser(userOld)] = user;
			return;
		}
	}
	public void deleteUser(String id) {
		User user = findUser(id);
	if (findUser(id) != null) {
		usersData[this.indexOfUser(user)] = user;
		registerdUser--;
		return;
	}
	}
	private int indexOfUser(User user) {
		for (int i=0; i < usersData.length; i++) {
			if (user.equals(usersData[i])) {
				return i;
			}
		}
		return -1;
	}
	}
		public User findSession(String id) {
			for (Session session : sessionsData) {
				if (session.getId().getText().equals(id)) {
					return session;
				}
			}
				return null;
			}
			public void createSession(Session session) {
				if (findUser(session.getId().getText() == null){
				UsersData[registerdUser] = session;
				registerdUser++;				
				return
				}
	}
	}